package com.example.fath.yamamlogin.callbacks;

/**
 * Created by fath_soleeklab on 2/15/2018.
 */

public interface ChangePhoneNumber {
    void onChangeNumber(int position);
}
