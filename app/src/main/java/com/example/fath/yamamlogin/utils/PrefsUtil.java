package com.example.fath.yamamlogin.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.fath.yamamlogin.YamamApplication;

public class PrefsUtil {


    private static final String DEFAULT_APP_PREFS_NAME = "yamam-default-prefs";

    private static SharedPreferences getPrefs() {

        return YamamApplication.get().getSharedPreferences(DEFAULT_APP_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static void saveString(String key, String value) {
        getPrefs().edit().putString(key, value).apply();
    }

    public static String getString(String key) {
        return getPrefs().getString(key, null);
    }


    public static void saveBoolean(String key, boolean value) {
        getPrefs().edit().putBoolean(key, value).apply();
    }


    public static boolean getBoolean(String key) {
        return getPrefs().getBoolean(key, false);
    }


    }