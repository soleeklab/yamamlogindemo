package com.example.fath.yamamlogin.callbacks;

/**
 * Created by fath_soleeklab on 2/7/2018.
 */

public interface OnEditClicked {
    void onClick (int position, String text, boolean isEdit);
}
