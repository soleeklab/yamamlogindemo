package com.example.fath.yamamlogin.core.constants;

public class PrefsConstants {

    public static final String USER_TOKEN = "userToken";
    public static final String USER_ID = "userId";
    public static final String USER_EMAIL = "userEmail";
    public static final String USER_NAME = "userName";
    public static final String USER_IMAGE = "userImage";
    public static final String IS_LOGGED_KEY ="isLogged" ;

}
