package com.example.fath.yamamlogin;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;


import com.example.fath.yamamlogin.Adapter.ChatMessageAdapter;
import com.example.fath.yamamlogin.Adapter.CountriesCodeAdapter;
import com.example.fath.yamamlogin.IntroViewModel;
import com.example.fath.yamamlogin.R;
import com.example.fath.yamamlogin.callbacks.ChangePhoneNumber;
import com.example.fath.yamamlogin.callbacks.OnCountrySelect;
import com.example.fath.yamamlogin.callbacks.OnEditClicked;
import com.example.fath.yamamlogin.model.ChatMessage;
import com.example.fath.yamamlogin.model.Country;
import com.example.fath.yamamlogin.utils.CountryDetails;
import com.example.fath.yamamlogin.utils.CredentialValidator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.internal.Utils;

import static com.example.fath.yamamlogin.Utils.getCountryDialCode;
import static com.example.fath.yamamlogin.Utils.showNoConnectionDialog;
import static com.example.fath.yamamlogin.core.constants.BundleConstants.LOG_TAG;
import static com.example.fath.yamamlogin.core.constants.BundleConstants.debug;

public class IntroActivity extends AppCompatActivity implements OnCountrySelect, ChangePhoneNumber, OnEditClicked, IntroInterface {
    @BindView(R.id.txt_sign)
    TextView signTv;
    @BindView(R.id.btn_signin)
    Button signinBtn;
    @BindView(R.id.btn_signup)
    Button signupBtn;
    @BindView(R.id.et_chat)
    EditText chatEt;
    @BindView(R.id.dimmed_view)
    View dimmedView;
    @BindView(R.id.txt_signup)
    TextView signupTv;
    @BindView(R.id.txt_signin)
    TextView singinTv;
    @BindView(R.id.rv_chat)
    RecyclerView chatRv;
    @BindView(R.id.layout_flags)
    LinearLayout flagsLayout;
    @BindView(R.id.txt_error)
    TextView errorTv;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.dimmed_list)
    ImageView listDimmed;
    @BindView(R.id.layout_chat)
    ConstraintLayout chatLayout;
    @BindView(R.id.txt_flag_id)
    TextView currentCodeTv;
    @BindView(R.id.flipper)
    ViewFlipper viewFlipper;
    @BindView(R.id.rv_countries)
    RecyclerView countriesRv;
    @BindView(R.id.et_code_search)
    EditText codeSearchEt;
    @BindView(R.id.txt_chat_anim)
    TextView animTv;
    @BindView(R.id.rv_scroll)
    NestedScrollView scrollView;
    CountriesCodeAdapter countriesCodeAdapter;
    public static final String SIGN_IN = "Sign in";
    public static final String SIGN_UP = "Sign up";
    private long lastClickTime = 0;
    String loginType = "";
    int messageCounter = 0;
    private ChatMessageAdapter mAdapter;
    boolean first = true;
    ArrayList<Country> countries = new ArrayList<>();
    String countryName, firstName, lastName, phoneNumer, countryCode, verifyCode;
    CountDownTimer countDownTimer;
    IntroViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);
        viewModel = new IntroViewModel(this);
        viewFlipper.setDisplayedChild(0);
        showNoConnectionDialog(this);
        mAdapter = new ChatMessageAdapter(this, new ArrayList<ChatMessage>(), this, this);
        //chatRv.setHasFixedSize(true);
        chatRv.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        ChatMessage firstMessage = new ChatMessage(getResources().getString(R.string.hi), ChatMessage.Sender.bot,false);
        mAdapter.add(firstMessage);
        codeSearchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (countriesCodeAdapter != null) {
                    countriesCodeAdapter.getFilter().filter(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setBackButton(View view) {
        viewFlipper.setDisplayedChild(0);
    }

    // country code click button
    public void setChooseCodeBtn(View view) {
        initializeCountries();
        countriesCodeAdapter = new CountriesCodeAdapter(countries, IntroActivity.this, IntroActivity.this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL);
        countriesRv.addItemDecoration(dividerItemDecoration);
        countriesRv.setAdapter(countriesCodeAdapter);
        viewFlipper.setDisplayedChild(1);
    }

    // signin button
    public void setSigninBtn(final View view) {
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();
        signupBtn.setClickable(false);
        loginType = SIGN_IN;
        if (first) sendMessage(loginType);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startButtonAnimation(view, singinTv);
                    }
                });
            }
        }, 50);
    }

    // signup button
    public void setSignupBtn(final View view) {
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000) {
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();
        signinBtn.setClickable(false);
        loginType = SIGN_UP;
        if (first) sendMessage(loginType);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startButtonAnimation(view, signupTv);
                    }
                });
            }
        }, 50);
    }

    public void setEditIv(View view) {
        messageCounter = 0;
        errorTv.setVisibility(View.INVISIBLE);

        switch (signTv.getText() + "") {
            case "Sign in":
                if (!first) signinBtn.performClick();
                Log.d("saif sign", "in");
                break;
            case "Sign up":
                if (!first) signupBtn.performClick();
                Log.d("saif sign", "up");
                break;
        }
        signupBtn.setClickable(true);
        signinBtn.setVisibility(View.VISIBLE);
        dimmedView.setVisibility(View.VISIBLE);
        signinBtn.setClickable(true);
        signupBtn.setVisibility(View.VISIBLE);
        toggleCaht(false);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        countDownTimer.cancel();
                        mAdapter.clear();
                        mAdapter = new ChatMessageAdapter(IntroActivity.this, new ArrayList<ChatMessage>(), IntroActivity.this, IntroActivity.this);
                        chatRv.setAdapter(mAdapter);
                        ChatMessage firstMessage = new ChatMessage(getResources().getString(R.string.hi), ChatMessage.Sender.bot,false);
                        mAdapter.add(firstMessage);
                        first = true;
                    }
                });
            }
        }, 60);
    }


    public void sendBtn(View view) {
        messageCounter++;
        sendMessage(chatEt.getText().toString());
        chatEt.getText().clear();
    }

    // start sign button animation.
    public void startButtonAnimation(View view, final TextView textView) {
        final AnimatorSet moveAnimation = new AnimatorSet();
        float y = (chatRv.getChildAt(1).getY() + toolbar.getHeight()) - view.getY();
        // button click animation
        if (first) {
            signTv.setText(textView.getText());
            final ObjectAnimator translateX = ObjectAnimator.ofFloat(view, "translationX", 0,
                    (signTv.getX() - textView.getX()));
            translateX.setDuration(600);

            final ObjectAnimator translateY = ObjectAnimator.ofFloat(view, "translationY", 0, y);
            translateY.setDuration(600);

            ObjectAnimator fadeOut = ObjectAnimator.ofFloat(view, View.ALPHA, 1f, 0f);
            fadeOut.setDuration(400);

            final ObjectAnimator translateTextX = ObjectAnimator.ofFloat(textView, "translationX", 0,
                    (signTv.getX() + signTv.getWidth()) - (textView.getX() + textView.getWidth()));
            translateTextX.setDuration(600);

            final ObjectAnimator translateTextY = ObjectAnimator.ofFloat(textView, "translationY", 0, y);
            translateTextY.setDuration(600);

            ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(view, "scaleX", 0.f);
            scaleDownX.setDuration(400);

            ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(view, "scaleY", 0.9f);
            scaleDownY.setDuration(400);

            ObjectAnimator fadeIn = ObjectAnimator.ofFloat(textView, View.ALPHA, 0f, 1f);
            fadeIn.setDuration(400);

            moveAnimation.playTogether(translateX, translateY, fadeOut, translateTextX, translateTextY, fadeIn, scaleDownX, scaleDownY);
            moveAnimation.start();

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            signinBtn.setVisibility(View.INVISIBLE);
                            signupBtn.setVisibility(View.INVISIBLE);
                            signupBtn.setClickable(false);
                            signinBtn.setClickable(false);
                            toggleCaht(true);
                            flagsLayout.setVisibility(View.GONE);
                            textView.setVisibility(View.INVISIBLE);
                            first = false;
                        }
                    });
                }
            }, 600);

        }
        //edit button animation.
        else {
            reverseButtonAnime(view, textView);
        }
    }

    private void reverseButtonAnime(View view, final TextView textView) {
        final AnimatorSet moveAnimation = new AnimatorSet();
        float y = (chatRv.getChildAt(1).getY() + toolbar.getHeight()) - view.getY();

        textView.setVisibility(View.VISIBLE);
        final ObjectAnimator translateX = ObjectAnimator.ofFloat(view, "translationX",
                (signTv.getX() - textView.getX()));
        translateX.setDuration(600);

        final ObjectAnimator translateY = ObjectAnimator.ofFloat(view, "translationY",
                y);
        translateY.setDuration(600);

        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(view, View.ALPHA, 0f, 1f);
        fadeOut.setDuration(500);

        final ObjectAnimator translateTextX = ObjectAnimator.ofFloat(textView, "translationX",
                (signTv.getX() + signTv.getWidth()) - (textView.getX() + textView.getWidth()));
        translateTextX.setDuration(600);

        final ObjectAnimator translateTextY = ObjectAnimator.ofFloat(textView, "translationY", y);
        translateTextY.setDuration(600);

        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        scaleDownX.setDuration(400);

        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(view, "scaleY", 1f);
        scaleDownY.setDuration(400);

        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(textView, View.ALPHA, 1f, 0f);
        fadeIn.setDuration(600);

        moveAnimation.playTogether(translateX, translateY, fadeOut, translateTextX, translateTextY, fadeIn, scaleDownX, scaleDownY);
        moveAnimation.start();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                textView.setAlpha(0);
            }
        }, 500);

    }

    // callback for edit button.
    @Override
    public void onClick(int position, String text, boolean isEdit) {
        switch (position) {
            case 1:
                com.example.fath.yamamlogin.Utils.hideSoftKeyboard(this, chatEt);
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.scrollTo(0, chatRv.getTop());
                                setEditIv(null);
                            }
                        });
                    }
                }, 50);
                break;
            default:
                if (isEdit) {
                    chatLayout.setVisibility(View.INVISIBLE);
                } else {
                    chatLayout.setVisibility(View.VISIBLE);
                }
                break;
        }
    }


    private void scrollMyListViewToBottom() {
        chatRv.post(new Runnable() {
            @Override
            public void run() {
                chatRv.scrollToPosition(mAdapter.getItemCount() - 1);
                scrollView.scrollTo(0, chatRv.getBottom());
            }
        });
    }

    // fill data to country model to send it to adapter
    public void initializeCountries() {
        for (int i = 0; i <
                CountryDetails.country.length; i++) {
            Country country = new Country(CountryDetails.code[i], CountryDetails.country[i]);
            countries.add(country);
        }
    }

    //callback when select country
    @Override
    public void onSelect(String code, int position) {
        viewFlipper.setDisplayedChild(0);
        currentCodeTv.setText(code);
    }

    // callback for change phone number button
    @Override
    public void onChangeNumber(int position) {
        if (position == -1) {
            mAdapter.removeLastItem();
            ChatMessage chatMessage1 = new ChatMessage(null, ChatMessage.Sender.bot,false);
            mAdapter.add(chatMessage1);

            if (loginType == SIGN_UP) {
                viewModel.signup_request(firstName, lastName, phoneNumer, countryCode);
            } else {
                viewModel.login_request(phoneNumer, "+" + currentCodeTv.getText().toString());
            }
            return;
        }
        if (loginType == SIGN_UP) {

            messageCounter = 2;
            if (position != 1) mAdapter.removeLastItem();
            toggleCaht(false);
            generateReplyMessage(null);

        } else {
            messageCounter = 0;
            if (position != 1) mAdapter.removeLastItem();
            toggleCaht(false);
            generateReplyMessage(null);
        }
    }

    //show and hide dimmed view in chat box
    public void toggleCaht(boolean active) {
        if (active) {
            errorTv.setVisibility(View.GONE);
            dimmedView.setVisibility(View.INVISIBLE);
            chatEt.setFocusableInTouchMode(true);
            chatEt.requestFocus();
        } else {
            flagsLayout.setVisibility(View.GONE);
            chatEt.setInputType(InputType.TYPE_CLASS_TEXT);
            scrollMyListViewToBottom();
            dimmedView.setVisibility(View.VISIBLE);
            chatEt.setFocusable(false);
        }
    }

    // validate user message then add it to the adapter
    public void sendMessage(final String message) {
        Log.d("send", message + loginType);
        errorTv.setVisibility(View.INVISIBLE);
        switch (loginType) {
            case SIGN_UP:
                switch (messageCounter) {
                    case 0:
                        /*ChatMessage chatMessage = new ChatMessage(message, ChatMessage.Sender.human,true);
                        messages.add(new ChatMessage(message, ChatMessage.Sender.human,true));
                        mAdapter.add(chatMessage);
                        scrollMyListViewToBottom();*/
                    case 1:
                        if (message.matches(String.valueOf(CredentialValidator.USER_NAME_PATTERN))) {
                            Log.d("saif", "true" + message);
                            addMssage(message,true);
                            firstName = message;
                        } else {
                            showErrorValidatioin(getResources().getString(R.string.wrong_name));
                            return;
                        }
                        break;
                    case 2:
                        if (message.matches(String.valueOf(CredentialValidator.USER_NAME_PATTERN))) {
                            addMssage(message,true);
                            lastName = message;
                        } else {
                            showErrorValidatioin(getResources().getString(R.string.wrong_name));
                            return;
                        }
                        break;
                    case 3:
                        if (message.matches(String.valueOf(CredentialValidator.PHONE_PATTERN))) {
                            addMssage("+" + currentCodeTv.getText().toString() + message,false);
                            toggleCaht(false);
                            startLoading();
                            countryCode = "+" + currentCodeTv.getText().toString();
                            phoneNumer = message;
                            if (!viewModel.signup_request(firstName, lastName, phoneNumer, countryCode))
                                return;
                        } else {
                            showErrorValidatioin(getResources().getString(R.string.wrong_phone));
                            return;
                        }
                        break;
                    case 4:
                        if (message.matches(String.valueOf(CredentialValidator.CODE_PATTERN))) {
                            verifyCode = message;
                            mAdapter.removeLastItem();
                            addMssage(message,false);
                            startLoading();
                            viewModel.verify_request(phoneNumer, "+" + currentCodeTv.getText().toString(), message);

                        } else {
                            showErrorValidatioin(getResources().getString(R.string.verfication_code));
                            return;
                        }
                        break;
                    default:
                        break;
                }
                break;
            case SIGN_IN:
                switch (messageCounter) {

                    case 1:
                        if (message.matches(String.valueOf(CredentialValidator.PHONE_PATTERN))) {
                            addMssage("+" + currentCodeTv.getText().toString() + message,false);
                            toggleCaht(false);
                            phoneNumer = message;
                            startLoading();
                            if (!viewModel.login_request(message, "+" + currentCodeTv.getText().toString())) {
                                return;
                            }

                        } else {
                            showErrorValidatioin(getResources().getString(R.string.wrong_phone));
                            return;
                        }
                        break;
                    case 2:
                        if (message.matches(String.valueOf(CredentialValidator.CODE_PATTERN))) {
                            verifyCode = message;
                            mAdapter.removeLastItem();
                            addMssage(message,false);
                            startLoading();
                            viewModel.verify_request(phoneNumer, "+" + currentCodeTv.getText().toString(), message);

                        } else {
                            showErrorValidatioin(getResources().getString(R.string.wrong_code));
                            return;
                        }
                        break;

                    default:
                        addMssage(message,true);
                        break;
                }
                break;
        }
        if (messageCounter == 0) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            toggleCaht(false);
                            generateReplyMessage(message);
                        }
                    });
                }
            }, 700);
        } else {
            toggleCaht(false);
            generateReplyMessage(message);
        }
    }

    //generate auto message responding to the user sent message
    public void generateReplyMessage(String message) {
        Log.d("generate", messageCounter + "");
        switch (loginType) {
            case "Sign up":
                if (messageCounter == 0) {
                    startLoading();

                    countDownTimer = new CountDownTimer(2000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {

                            finishLoading(getResources().getString(R.string.first_name));
                        }
                    }.start();
                }
                switch (messageCounter) {
                    case 1:
                        startLoading();
                        countDownTimer = new CountDownTimer(2000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                finishLoading(getResources().getString(R.string.last_name));
                            }
                        }.start();

                        break;
                    case 2:
                        startLoading();
                        countDownTimer = new CountDownTimer(2000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                finishLoading(getResources().getString(R.string.phone_number));
                                togglePhoneView(true);
                            }
                        }.start();

                        break;
                    case 3:
                        startLoading();
                        countDownTimer = new CountDownTimer(2000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                finishLoading(getResources().getString(R.string.verfication_code));
                                mAdapter.add(ChatMessage.progressBar);
                                scrollMyListViewToBottom();

                            }
                        }.start();

                        break;
                }
                break;
            case "Sign in":
                if (messageCounter == 0) {
                    startLoading();
                    countDownTimer = new CountDownTimer(2000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                        }

                        @Override
                        public void onFinish() {
                            finishLoading(getResources().getString(R.string.phone_number));
                            togglePhoneView(true);

                        }
                    }.start();
                }

                switch (messageCounter) {
                    case 1:
                        startLoading();
                        new Timer().schedule(new TimerTask() {
                            @Override
                            public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        finishLoading(getResources().getString(R.string.verfication_code));
                                        mAdapter.add(ChatMessage.progressBar);
                                    }
                                });
                            }
                        }, 2000);
                        break;
                }
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void successAuth() {
        // chatRv.removeAllViews();
//  mAdapter.clear();

        //mAdapter = new ChatMessageAdapter(IntroActivity.this, messages, IntroActivity.this, IntroActivity.this);
        for (int i =2;i<mAdapter.data.size();i++){

            mAdapter.data.get(i).setEditable(false);
            mAdapter.notifyDataSetChanged();

        }
//        chatRv.setAdapter(mAdapter);
        //   chatRv.swapAdapter(mAdapter,true);
        mAdapter.removeLastItem();
        generateReplyMessage("");

    }

    @Override
    public void sucessVerify() {
        Intent intent = new Intent(IntroActivity.this, MainActivity.class);
        mAdapter.removeLastItem();
        startActivity(intent);
        finish();
    }

    @Override
    public void sendErrorMessage(String error) {
        if (debug) Log.d(LOG_TAG + "senderror", error);
        ChatMessage chatMessage = new ChatMessage(error, ChatMessage.Sender.bot,false);
        if (!error.contains("Invalid verification code")) {
            mAdapter.removeLastItem();
            mAdapter.add(chatMessage);
            toggleCaht(true);
            togglePhoneView(true);
            if (loginType == SIGN_UP) {
                messageCounter = 2;
            } else {
                messageCounter = 0;
            }
        } else {
            mAdapter.removeLastItem();
            mAdapter.add(chatMessage);
            toggleCaht(true);
            if (loginType == SIGN_UP) {
                messageCounter = 3;
            } else {
                messageCounter = 1;
            }
        }
        scrollMyListViewToBottom();
    }

    //send request for sign up with name and phone number

    //vm
    private void finishLoading(String message) {
        mAdapter.removeLastItem();
        ChatMessage chatMessage = new ChatMessage(message, ChatMessage.Sender.bot,false);
        mAdapter.add(chatMessage);
        togglePhoneView(false);
        scrollMyListViewToBottom();
        toggleCaht(true);
    }

    private void startLoading() {
        ChatMessage chatMessage = new ChatMessage(null, ChatMessage.Sender.bot,false);
        mAdapter.add(chatMessage);
        scrollMyListViewToBottom();
    }

    private void togglePhoneView(boolean isView) {
        if (isView) {
            flagsLayout.setVisibility(View.VISIBLE);
            currentCodeTv.setText(getCountryDialCode(IntroActivity.this));
            Log.d("code", CountryDetails.code.length + "/" + CountryDetails.country.length);
            chatEt.setInputType(InputType.TYPE_CLASS_PHONE);
        } else {
            chatEt.setInputType(InputType.TYPE_CLASS_TEXT);
            flagsLayout.setVisibility(View.GONE);
        }
    }

    private void showErrorValidatioin(String message) {
        errorTv.setText(message);
        errorTv.setVisibility(View.VISIBLE);
        messageCounter--;
    }

    private void addMssage(String message,boolean isEditable) {
        ChatMessage chatMessage = new ChatMessage(message, ChatMessage.Sender.human,isEditable);
        mAdapter.add(chatMessage);
        scrollMyListViewToBottom();
    }
}
